# Dépot lié à la Webradio des Ceméa

Ce dépot contient plusieurs évolutions du projet Webradio

Le projet initiale est une adaptation de la distribution Studiobox3, réalisée par l'Académie de Versailles à destination des lycéens.
https://linuxfr.org/news/studiobox-3-couteau-suisse-multimedia-pour-webradio-scolaire
Ce projet a été adapté pour une diffusion vers le serveur webradio des Ceméa.
https://radios.cemea.org


## Différentes versions ou évolutions du projet Webradio

Elles sont stockées dans différents dossiers de ce dépot :

### Studiobox3

Version initiale basée sur Debian Jessie 8 - adaptée pour les Ceméa en V 1.0. Version fonctionnelle

### Studiobox4

Version basée basée sur Debian Strech 9, dispo uniquement sur i386. Développement jamais abouti ?

### Studiobox5

Version basée sur Debian Buster 10. En construction, basée sur les outils simples de Studiobox3 et 4. Mise en test par l'académie de Versailles.

### Studiobox5.11

Version de Studiobox basé sur Debian Bullseye 11, basée sur les outils de la Studiobox3 et 4. Développement en cours par les Ceméa

### Webradio1.2203

* Mise en place d'une webradio améliorée sur base d'une Debian Bullseye avec IDJC et Jackd issus de dépots Librazik. 
* Projet de webradio adapté pour les CEMEA Belges puis les CEMEA FR
* Version évoluée avec environnement MATE, plus complet mais à la configuration plus complexe. https://ladoc.cemea.org/technique/webradiov2_librazik#parametrage_de_jackd




