# Studiobox CEMEA
Adaptée de studiobox 3 basée sur le projet :
http://logicielslibres.dane.ac-versailles.fr/spip.php?article270
Distribution Linux basée sur Debian Live Jessie

Projet sous Licence GNU/GPL

## Projet et Documenation

* Projet webradio : https://radios.cemea.org

* Documentation disponible sur https://ladoc.cemea.org

## Contributeurs/testeurs

* François Audirac
* Pascal Gascoin
* Laurent Bessonnet
* Romain Renaud pour la partie web 

## Version 1.0

### Fonctionnalités

2018-03-20

* Correction : suppression de la page par défaut dans /var/www/html/index.html dans hooks
* Suppression des entrées de menu "interface d'administration"
* Ajout de l'entrée "Diffuser et mixer" vers mixxx
* Correction du script diffrec-LS.bash pour installation airtime vers diffusion mp3
* Correction du lien dans Raccourcis Firefox vers page locale http://localhost

2018-02-07

* Personnalisation de la page d'accueil Webradio interne index.php
* Changement de nom du flux audio local : webradio.mp3
* Modification dans /etc/icecast2/icecast.xml
* Modification/corrections du .Scripts/diffrec-LS.bash : retrait du mixer par défaut

2018-02-06

* Ajout d'un menu pour affichage d'icones
* Ajout du lancement du mixer pour chaque diffusion
* Correction bug diffusion externe

2018-01-02

* Ajout d'une interface avec série d'icones
* Icone pour extinction
* Raccourci F10 pour affichage du menu (en complément du clic-droit)
* Fond d'écran pour affichage du F10 menu

2017-12-27

* Ajout de paquets complémentaires : qasmixer, idesk

2017-12-13

* Réduction des paquets limités à la diffusion Radio (suppression des paquets TV)
* Configuration pour diffusion sur le canal des CEMEA : https://radios.cemea.org
* Mise à jour de la documentation
* Corrections bugs mineurs
* Désactivation du partitionnement automatique pressed
* Correction bug sur port PostgreSQL dans instal-airtime
* Suppression des paquets video : kdenlive...
