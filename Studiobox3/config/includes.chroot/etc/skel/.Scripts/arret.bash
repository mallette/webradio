#!/bin/bash

function annulzen {
	# possibilité d'annuler la config en cliquant sur annuler
	if [ "$?" -eq 1 ]; then
    		exit
	fi
}

function arretQ {
# formulaire zenity
	zenity --question --title="Eteindre" --text="Voulez-vous 
éteindre ?" 2>/dev/null
	if [ $? = 0 ]; then
		sudo /usr/local/bin/arret2
	fi
}

arretQ
