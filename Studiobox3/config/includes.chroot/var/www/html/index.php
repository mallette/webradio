<?php

// $localIP = getHostByName(getHostName());

$localIP=$_SERVER['SERVER_ADDR'];
$localPort="8000";

print '
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>WebRadio interne CEMEA</title>
    <style type="text/css" media="screen">
  * {
    margin: 0px 0px 0px 0px;
    padding: 0px 0px 0px 0px;
  }

  body, html {
    padding: 3px 3px 3px 3px;

    background-color: #D8DBE2;

    font-family: Verdana, sans-serif;
    font-size: 11pt;
    text-align: center;
  }

  div.main_page {
    position: relative;
    display: table;

    width: 800px;

    margin-bottom: 3px;
    margin-left: auto;
    margin-right: auto;
    padding: 0px 0px 0px 0px;

    border-width: 2px;
    border-color: #212738;
    border-style: solid;

    background-color: #FFFFFF;

    text-align: center;
  }

  div.page_header {
    height: 99px;
    width: 100%;

    background-color: #FFF;
  }

  div.page_header span {
    margin: 15px 0px 0px 50px;

    font-size: 180%;
    font-weight: bold;
  }

  div.page_header img {
    margin: 3px 0px 0px 40px;

    border: 0px 0px 0px;
  }

  div.table_of_contents {
    clear: left;

    min-width: 200px;

    margin: 3px 3px 3px 3px;

    background-color: #FFFFFF;

    text-align: left;
  }

  div.table_of_contents_item {
    clear: left;

    width: 100%;

    margin: 4px 0px 0px 0px;

    background-color: #FFFFFF;

    color: #000000;
    text-align: left;
  }

  div.table_of_contents_item a {
    margin: 6px 0px 0px 6px;
  }

  div.content_section {
    margin: 3px 3px 3px 3px;

    background-color: #FFFFFF;

    text-align: left;
  }

  div.content_section_text {
    padding: 4px 8px 4px 8px;

    color: #000000;
    font-size: 100%;
  }

  div.content_section_text pre {
    margin: 8px 0px 8px 0px;
    padding: 8px 8px 8px 8px;

    border-width: 1px;
    border-style: dotted;
    border-color: #000000;

    background-color: #F5F6F7;

    font-style: italic;
  }

  div.content_section_text p {
    margin-bottom: 6px;
  }

  div.content_section_text ul, div.content_section_text li {
    padding: 4px 8px 4px 16px;
  }

  div.section_header {
    padding: 3px 6px 3px 6px;

    background-color: #8E9CB2;

    color: #FFFFFF;
    font-weight: bold;
    font-size: 112%;
    text-align: center;
  }

  div.section_header_red {
    background-color: #CD214F;
  }

  div.section_header_grey {
    background-color: #9F9386;
  }

  .floating_element_left {
    position: relative;
    float: left;
  }

  .floating_element_right {
    position: relative;
    float: right;
  }

  div.table_of_contents_item a,
  div.content_section_text a {
    text-decoration: none;
    font-weight: bold;
  }

  div.table_of_contents_item a:link,
  div.table_of_contents_item a:visited,
  div.table_of_contents_item a:active {
    color: #000000;
  }

  div.table_of_contents_item a:hover {
    background-color: #000000;

    color: #FFFFFF;
  }

  div.content_section_text a:link,
  div.content_section_text a:visited,
   div.content_section_text a:active {
    background-color: #DCDFE6;

    color: #000000;
  }

  div.content_section_text a:hover {
    background-color: #000000;

    color: #DCDFE6;
  }

  div.validator {
  }
    </style>
  </head>
  <body>
    <div class="main_page">
      <div class="page_header floating_element">
        <img src="radios.png" height="80" alt="Webradio" class="floating_element_left"/>
        <img src="logocemea.png" height="80" alt="CEMEA" class="floating_element_right"/>

        <span class="floating_element">
          WebRadio interne CEMEA
        </span>
      </div>
      <div class="content_section floating_element">


        <div class="section_header section_header_red">
          <div id="about"></div>
          Ecoutez-nous !
        </div>
        <div class="content_section_text">
	  <p>
		<h2>Ecouter en direct ( <a href="https://'.$localIP.':'.$localPort.'/webradio.mp3">MP3</a> )</h2>
		<br/>
		<div align="center">
		<audio controls>
			<source src="http://'.$localIP.':'.$localPort.'/webradio.mp3">
		</audio>
		</div>
	  </p>

        </div>
        <div class="section_header">
          <div id="changes"></div>
                Qu\'est-ce qu\'une webradio ?
        </div>
        <div class="content_section_text">
          <p>
                Une webradio est une radio diffusée par le biais d\'Internet.
		Vous êtes actuellement en train d\'écouter une radio diffusée uniquement sur le réseau local de ce lieu. Il n\'est pas acccessible depuis l\'extérieur.
		Vous pouvez tout de même ecouter cette radio et profiter de ce service tant que vous restez connecté au même réseau wifi que la radio. 
          </p>
        </div>

        <div class="section_header">
            <div id="docroot"></div>
                Les CEMÉA
        </div>

        <div class="content_section_text">
            <p>
                Les <a href="https://cemea.asso.fr">Centres d\'Entrainement aux Méthodes d\'Éducation Active</a> est une association regroupant des militants et militantes oeuvrant pour l\'Education Nouvelle.
		Ils vous permettent de bénéficier de ce service disponible aussi pour les formateurs et animateurs affilliés aux CEMEA.
		<ul>
			<li>Voir le site officiel <a href="https://radios.cemea.org">Radios CEMEA</a> avec un formulaire de demande si vous êtes concernés.</li>
		</ul> 

            </p>
        </div>

        <div class="section_header">
          <div id="bugs"></div>
                Documentation et Problèmes
        </div>
        <div class="content_section_text">
          <p>
		<ul>
			<li>Voir la page de documentation pour en savoir plus sur le <a href="https://ladoc.cemea.org">projet technique</a> et faire remonter vos propositions, bugs...</li>
			<li>Le système utilisé est basé sur des logiciels libres, tiré du projet initial <a href="http://logicielslibres.dane.ac-versailles.fr/spip.php?article270">Studiobox</a>, il peut-être copié, modifié, amélioré...
		</ul>
        </div>
      </div>
    </div>
    <div class="validator">
    </div>
  </body>
</html>';

?>


