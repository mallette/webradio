.$GLOBALAUTOSTART

export OOO_FORCE_DESKTOP=gnome

nitrogen --restore &
tint2 &
volumeicon &
sleep 3
conky &
xset -dpms &
xset s noblank &
xset s off &
bash $HOME/.Scripts/menupersistence.sh &
bash .Scripts/conkfig.sh &

