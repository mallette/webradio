#!/bin/bash

function annulzen {
# possibilité d'annuler la config en cliquant sur annuler
if [ "$?" -eq 1 ]; then
    exit
fi
}

function supprPG {
A=$(crontab -l | awk '{print $1 $2 $3 $4 $5 $9}')
echo $A
zenity --entry --title="Jour" --text="Sélectionnez le programme à supprimer" $A
}
annulzen
